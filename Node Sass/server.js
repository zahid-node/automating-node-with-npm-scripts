
var express = require('express');
var path = require('path');
var port = process.env.npm_package_config_port || 3000;
var app = express();

app.use(express.static(path.join(__dirname,'public')));

app.get('/', function (req, res) {
    res.render('./index.html');
});
app.listen(port, function () { console.log("Server is up and running on port : " + port); });
