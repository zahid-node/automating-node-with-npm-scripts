
var express = require('express');
var path = require('path');
var port = process.env.npm_package_config_port || 3000;
var app = express();

app.use(express.static(path.join(__dirname,'./')));

app.get('/', function (req, res) {
    res.send('Hello from node with webpack');
});
app.listen(port, function () { console.log("Server is up and running on port : " + port); });

module.exports = app;
