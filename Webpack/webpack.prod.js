const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const outputDirectory = path.resolve(__dirname,'dist');
const entry = path.join(__dirname,'./src/client','client.js');

module.exports = {
    mode: 'production',
    entry: entry,
    output: {
        filename: 'bundle.js',
        chunkFilename: '[name].vendor.js',
        path:outputDirectory,
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin([
            {from:'./src/client/**/*', to:outputDirectory , flatten:true,ignore:['*.js','*.sass']},
            {from:'./src/server/**/*', to:outputDirectory + '/server.js' , flatten:true}
        ],{copyUnmodified:true})
    ]
};