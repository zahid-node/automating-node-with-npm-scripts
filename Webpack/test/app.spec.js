const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

const app = require('../src/server/server');

describe('GET /',function(){
    it('should return with a message Hello from node with webpack', function(done){
        chai.request(app)
            .get('/')
            .end(function(error,res){
                if(error){
                    return done(error);
                }
                expect(res.status).to.equal(200);
                expect(res.text).to.equal('Hello from node with webpack');
                done();
            })
    })
})