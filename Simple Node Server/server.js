
var express = require('express');
var path = require('path');
var port = process.env.npm_package_config_port || 3000;
var app = express();


app.get('/', function (req, res) {
    res.send(`Hello from simple node server`);
});
app.listen(port, function () { console.log("Server is up and running on port : " + port); });

module.exports = app;