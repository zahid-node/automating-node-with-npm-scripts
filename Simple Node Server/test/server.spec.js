const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = chai.expect;
chai.use(chaiHttp);

const app = require('../server');

describe('GET /',function(){
    it('should return with a message Hello from simple node server', function(done){
        chai.request(app)
            .get('/')
            .end(function(error,res){
                if(error){
                    return done(error);
                }
                expect(res.status).to.equal(200);
                expect(res.text).to.equal('Hello from simple node server');
                done();
            })
    })
})