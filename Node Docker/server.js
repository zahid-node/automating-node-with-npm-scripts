const express = require('express');
const port = process.env.port || 4005;
const app = express();

app.get('/',(req,res) => {
    res.send('Hello from Node inside the docker container');
});

app.listen(port,() => {console.log(`Server is running on port : ${port} and in docker with pluralsight`)})