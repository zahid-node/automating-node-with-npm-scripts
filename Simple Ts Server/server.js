"use strict";
var express = require('express');
var path = require('path');
var port = process.env.npm_package_config_port || 4000;
var app = express();
app.use(express.static(path.resolve("" + __dirname, './public')));
app.get('/', function (req, res) {
    res.send('Hello from simple TypeScript server Hope you are good');
});
app.listen(port, function () { console.log("Server is up and running on port : " + port); });
