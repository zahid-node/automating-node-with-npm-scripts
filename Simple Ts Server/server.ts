const express = require('express');
const path = require('path');
const port = process.env.npm_package_config_port || 4000;
const app = express();

app.use(express.static(path.resolve(`${__dirname}`,'./public')));
app.get('/',(req:any , res:any) => {
    res.send('Hello from simple TypeScript server Hope you are good');
});

app.listen(port,() => {console.log(`Server is up and running on port : ${port}`)});